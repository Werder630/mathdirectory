package com.example.mathdirectory;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	public DBHelper(Context context) {
		super(context, "Math.db", null, 1);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		db.execSQL("create table clients ("
		          + "id integer primary key autoincrement," 
		          + "name text,"
		          + "surname text," 
		          + "phone integer," 
		          + "vklink text," 
		          + "adress text" 
		          + ");");
		
	 
		db.execSQL("create table orders ("
		          + "id integer primary key autoincrement," 
		          + "theme text,"
		          + "variant text," 
		          + "excers text," 
		          + "client integer," 
		          + "dateorder date," 
		          + "datecomplete date," 
		          + "price int" 
		          + ");");
	
	}
	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		
	}

}
