package com.example.mathdirectory.orders;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.example.mathdirectory.MyDB;
import com.example.mathdirectory.R;

 
public class Orders extends Fragment implements OnClickListener{ 

	View mainview, button_test;
	static TableLayout tablelayout;
	MyDB mydb;
	protected static String[] name_field = new String[] {"theme", "variant", "excers", "client", "dateorder", "datecomplete", "price"};
 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState) {
	
		        mainview = inflater.inflate(R.layout.order, null);
		        tablelayout = (TableLayout)mainview.findViewById(R.id.main_table);	 
		        button_test = mainview.findViewById(R.id.test_order_button);
		        button_test.setOnClickListener(this);
		        mydb = new MyDB(getActivity());
				return mainview;		    
	}
	 
 
	
 
	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
		case R.id.test_order_button:
		  {   
			  AddOrderDialog.getValueOf(getActivity()).createAddOrderDialog();
			  refreshOrdersTable();
			  
			  break;
		  }
		}
		
	}




	private void refreshOrdersTable() {
		
		Cursor cursor = mydb.getAllDataFromTable(MyDB.ORDER_TABLE_NAME);
		cursor.moveToFirst();
		List<String> list = new ArrayList<String>();
		
		while (!cursor.isAfterLast())   {
  	      
			
			cursor.moveToNext();
            
  	//     tablelayout.addView(new MyTableRow(getActivity()).getTableRow(list));
        }
		
		
	}
	
}
