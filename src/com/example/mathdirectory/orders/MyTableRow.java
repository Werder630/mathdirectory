package com.example.mathdirectory.orders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.mathdirectory.clients.Clients;

import android.content.Context;
import android.graphics.Color;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class MyTableRow implements View.OnCreateContextMenuListener, OnMenuItemClickListener  {

	Context context;
	int [] weight_textview = new int [] {1, 3, 1, 3, 3, 2};
	String [] colorRow = new String [] {"#E9D8B0", "#EDD396"};
	public static int rowCount = 0;
	LinearLayout currentLL;
	Map<LinearLayout, TableRow> map = new HashMap<LinearLayout, TableRow>();
	
	MyTableRow(Context con)
	{context =  con;}
	
	
	/**
	 *  ����� ��� ���������� ������ � �������
	    @param count ���������� ����� � �������
	 */
	public TableRow getTableRow(List<String> textRowArray)
	{   
		TableRow tr = new TableRow(context);
		LinearLayout ll = new LinearLayout(context);
		ll.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
	    		TableRow.LayoutParams.WRAP_CONTENT));
		
	//	ll.setBackgroundColor(Color.parseColor(colorRow[count%2]));
		ll.setOnCreateContextMenuListener(this);
		ll.setId(rowCount);		
		
		for (int i = 0; i <= 5; i++)
		{TextView tv = new TextView(context);
	            tv.setLayoutParams(new LinearLayout.LayoutParams(
	            		0, 
	            		LinearLayout.LayoutParams.WRAP_CONTENT, 
	            		weight_textview[i]));
	            tv.setPadding(0, 5, 0, 5);
	    	    tv.setGravity(Gravity.CENTER_HORIZONTAL);
	    	    tv.setTextColor(Color.BLACK);
	    	    tv.setText(textRowArray.get(i));
	    ll.addView(tv); }
		
		tr.addView(ll);		
	    map.put(ll, tr);
		
		return tr;
	}
 

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		    currentLL = (LinearLayout)v;
		    menu.setHeaderTitle("����� � "+v.getId());
		    menu.add(0, 0, 0, "�������������").setOnMenuItemClickListener(this);
		    menu.add(0, 1, 0, "�������").setOnMenuItemClickListener(this);
		    menu.add(0, 2, 0, "����������").setOnMenuItemClickListener(this);
		
	}


	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId())
		{
		case 0:
		  {   Clients.prt("��� �������������");
			  break;
		  }
		  
		case 1:
		  {   Clients.prt("��� �������");
			  Orders.tablelayout.removeView(map.get(currentLL));
		      break;
		  }
		  
		case 2:
		  {   Clients.prt("��� ����������");
			  break;
		  }
		  
		default: break;
		 
		
		}
		return true;
	}
	
	

	 
}
