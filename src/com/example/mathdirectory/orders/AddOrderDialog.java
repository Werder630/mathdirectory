package com.example.mathdirectory.orders;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mathdirectory.MyDB;
import com.example.mathdirectory.R;

public class AddOrderDialog implements View.OnClickListener{
 
	Context context;
	public static Map<Integer, View> fieldOfDialog = new HashMap<Integer, View> ();
	public static AddOrderDialog thisinstence;
	View viewdialog;
	MyDB mydb;
	
	
	/**
	 * 
	 * �������� ������ ���������� �������
	 * @return ���������� ��������� ������ ������� AddOrderDialog
	 */
	static AddOrderDialog getValueOf(Context con)
	{   
		if (thisinstence == null) 
		{thisinstence = new AddOrderDialog(con);}
		return thisinstence;
	}
	

	private AddOrderDialog(Context con)
	{this.context = con;
	mydb = new MyDB(context);
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////����������� �����
///////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * ����� ������� ������ ������ ������
	 */
	public void createAddOrderDialog()
	{AlertDialog.Builder addorder_dialog = new AlertDialog.Builder(context);
	addorder_dialog.setTitle("���������� ������");
	View viewdialog = ((Activity)context).getLayoutInflater().inflate(R.layout.order_addorder, null);	
    initFieldDialog(viewdialog);
	addorder_dialog.setView(viewdialog);
	addorder_dialog.setPositiveButton("�������", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			putValuesToDB();
			
		}
	});
	addorder_dialog.setNegativeButton("������", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			dialog.cancel();
			
		}
	});
	
	addorder_dialog.create();
	addorder_dialog.show();
		
	} 
	
////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * ���������� ���������� fieldOfDialog ������� ��� �������� ������ � ����
	 * @param v View � ������ ��� ���������� ������ ������
	 */
	private void initFieldDialog(View v)
	{
		fieldOfDialog.put(0, v.findViewById(R.id.order_dialog_addorder_theme_et));
		fieldOfDialog.put(1, v.findViewById(R.id.order_dialog_addorder_variant_et));
		fieldOfDialog.put(2, v.findViewById(R.id.order_dialog_addorder_excers_et));
		fieldOfDialog.put(3, v.findViewById(R.id.order_dialog_addorder_client_et));
		fieldOfDialog.put(4, v.findViewById(R.id.order_dialog_addorder_dateorder_et));
		fieldOfDialog.put(5, v.findViewById(R.id.order_dialog_addorder_datecomplete_et));
		fieldOfDialog.put(6, v.findViewById(R.id.order_dialog_addorder_price_et));
	    
		//������������� ��� ��� � ������� ��������� ������
		fieldOfDialog.get(4).setOnClickListener(this);		
		fieldOfDialog.get(5).setOnClickListener(this);
		((Spinner)(fieldOfDialog.get(3))).setAdapter(new ArrayAdapter<String>(this.context, 
	    		   android.R.layout.simple_list_item_1, mydb.getNameListFromClientsTable()));
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////	
	/**
	 * �������� �������� � ���� ������
	 */
	void putValuesToDB()
	{
		ContentValues cv = new ContentValues();
		for (int i = 0; i < fieldOfDialog.size(); i++)
		{   //��� ������� ����� ������� �� ������� ID  
			if (i == 3) 
		           {cv.put(Orders.name_field[i], 
		        		   mydb.tempID.get((int)((Spinner)fieldOfDialog.get(3)).getSelectedItemId()));
		           }
		    else {cv.put(Orders.name_field[i], ((TextView)fieldOfDialog.get(i)).getText().toString()); }
		}
		mydb.getWriteDB().insert(MyDB.ORDER_TABLE_NAME, null, cv);
		mydb.close();
		((Spinner)fieldOfDialog.get(3)).getSelectedItemId();
	}



////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
		   case R.id.order_dialog_addorder_dateorder_et:
		   {   //�������� ������ ���� ��� ����� ���������� ����
			   getDateFromPicker(v);
			   break;
    		}
		   
		   case R.id.order_dialog_addorder_datecomplete_et:
		   {   //�������� ������ ���� ��� ����� ���������� ����
			   getDateFromPicker(v);
			   break;
    		}
		}
		
	}

 
    //���������� ���� �� DatePicker//////////////////////////////////////////
	private void getDateFromPicker(final View v) {
	
		Calendar c = Calendar.getInstance();

		DatePickerDialog dp = new DatePickerDialog(context, new OnDateSetListener() {

		    public void onDateSet(DatePicker view, int year, int monthOfYear,
		        int dayOfMonth) {
		    	//��������� ������ �� 
		    	 ((TextView)v).setText(""+dayOfMonth+" "+new DateFormatSymbols().getMonths()[monthOfYear]+" "+year);
		    	 
		      }
		    }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		
		dp.show();	
	}
	
	
	
	
}
