package com.example.mathdirectory;

import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.mathdirectory.clients.Clients;
import com.example.mathdirectory.orders.AddOrderDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveId;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;



public class MainActivity extends FragmentActivity 
implements  OnConnectionFailedListener, ConnectionCallbacks

{	
	static GoogleAccountCredential credential;
    Drive service;
    public DriveId driveid;
    public static GoogleApiClient mGoogleApiClient;
    TabHost th;
    final private String ISCONNECT = "#40FA97", NOTCONNECT = "#FC4852";
	
	@Override
	 protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.activity_main);

    	mGoogleApiClient = new GoogleApiClient.Builder(this)
        .addApi(Drive.API)
        .addScope(Drive.SCOPE_FILE)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .build();
    	
    	mGoogleApiClient.connect();
     
  	
    	th = (TabHost)findViewById(android.R.id.tabhost);
    	
    	th.setup();
    	
    	
    	
		th.getTabWidget().setBackgroundColor(Color.parseColor(this.NOTCONNECT));
				
    	TabHost.TabSpec tSpec;
    	tSpec = th.newTabSpec("t1");
    	tSpec.setIndicator("���������");
    	tSpec.setContent(R.id.tab1);
    	th.addTab(tSpec);
 
    	tSpec = th.newTabSpec("t2");
    	tSpec.setIndicator("������");
    	tSpec.setContent(R.id.tab2); 
    	th.addTab(tSpec);
    	
    	tSpec = th.newTabSpec("t3");
    	tSpec.setIndicator("�������");
    	tSpec.setContent(R.id.tab3);
    	th.addTab(tSpec); 	    
    	
    	for(int i = 0; i <= 2; i++)
    	{TextView t = (TextView)th.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
    	t.setTextColor(Color.GRAY);}
    } 
	
	@Override 
	protected void onDestroy()
	{   //������������� ����� �� �������� ����� �����������
		super.onDestroy();
		AddOrderDialog.thisinstence = null;
	}
	
	
	  	  
		@Override
		public void onConnected(Bundle arg0) {
			Clients.prt("�������");
			th.getTabWidget().setBackgroundColor(Color.parseColor(this.ISCONNECT));
		}

		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			Clients.prt("onConnectionFailed");			
			if (!arg0.hasResolution()) {
	            GooglePlayServicesUtil.getErrorDialog(arg0.getErrorCode(), this, 0).show();
	            return;
	        }
	        try {
	        	arg0.startResolutionForResult(this, 1);
	        } catch (SendIntentException e) {
	        }
		}
		
		 @Override
		    protected void onActivityResult(int requestCode, int resultCode,
		            Intent data) {
		        super.onActivityResult(requestCode, resultCode, data);
		        this.setResult(resultCode);
		    }

		@Override
		public void onConnectionSuspended(int arg0) {
			th.getTabWidget().setBackgroundColor(Color.parseColor(this.NOTCONNECT));
			
		}

	
	  
}
