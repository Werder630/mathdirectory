package com.example.mathdirectory.materials;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;

import com.example.mathdirectory.MainActivity;
import com.example.mathdirectory.clients.Clients;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveApi.ContentsResult;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveFolder.DriveFolderResult;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResource.MetadataResult;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filter;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;



public class DriveHelper extends MainActivity  {
	 
	Context context;
    public static DriveId driveidSelectFile;
    static boolean isDownloadFiles;
    static int countImage, currentCountImage;
    ProgressDialog pd;
    Handler h;

////�����������//////////////////////////////////////////////////////////////////////////////////
	public DriveHelper(Context con)
	{this.context = con;
	}
	
/////�������� ����� TitleOfFolder � ����� nameinfolder///////////////////////////////////////////////////////////////
	 void createMaterialFolder(String TitleOfFolder, String nameinfolder)
	 { 
		 MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
         .setTitle(TitleOfFolder).build();
        Drive.DriveApi.getFolder(MainActivity.mGoogleApiClient, getIDOfFolder(nameinfolder)).createFolder(
        		MainActivity.mGoogleApiClient, changeSet);     
	 }
	
///���������� ID ����� �� �����//////////////////////////////////////////////////////////
	public DriveId getIDOfFolder(String foldername) {
		  Filter filtr = Filters.and(
			          Filters.eq(SearchableField.MIME_TYPE, DriveFolder.MIME_TYPE),
			          Filters.eq(SearchableField.TITLE, foldername),
			          Filters.eq(SearchableField.TRASHED, false));
		  
		  Query qry = new Query.Builder().addFilter(filtr).build(); 
		  MetadataBufferResult rslt = Drive.DriveApi.query(MainActivity.mGoogleApiClient, qry).await();
		  MetadataBuffer mdb;  
		  if (rslt.getStatus().isSuccess()) {
		        mdb = null; }
		        mdb = rslt.getMetadataBuffer();  
 		return  mdb.get(0).getDriveId();
	}
	
	
////���������� ������ ����� � ����� �����//////////////////////////////////////////////////////////	
	public void getMetodsList(final String folderTitle, final int x)
	{    	
		
		PendingResult<DriveApi.MetadataBufferResult> pr = Drive.DriveApi.getFolder(MainActivity.mGoogleApiClient, getIDOfFolder(folderTitle)).listChildren(mGoogleApiClient);
		MetadataBufferResult mdbr = pr.await();
		
		if (x == 1)
		{Materials.metodsList.clear();
			for (int i = 0; i < mdbr.getMetadataBuffer().getCount(); i++)
		   {   
			Materials.metodsList.add(mdbr.getMetadataBuffer().get(i).getTitle());
		   } 
	    }
		
		else if (x == 2)
		{Materials.variantsList.clear();
			for (int i = 0; i < mdbr.getMetadataBuffer().getCount(); i++)
		   {   
			Materials.variantsList.add(mdbr.getMetadataBuffer().get(i).getTitle());
			Materials.variantsListDriveID.add(mdbr.getMetadataBuffer().get(i).getDriveId());
		   } 
	    }		
		
	}
	
////��������� ����� �� ����/////////////////////////////////////////////////////////////////////////
	void uploadFileToDrive(final Bitmap bitmap, final String title)
	{
		Drive.DriveApi.newContents(MainActivity.mGoogleApiClient).setResultCallback(new ResultCallback<ContentsResult>() {

			@Override
			public void onResult(ContentsResult res) {
			   	 OutputStream outputStream = res.getContents().getOutputStream();
	                ByteArrayOutputStream bitmapStream = new ByteArrayOutputStream();
	                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bitmapStream);
	                try {
	                    outputStream.write(bitmapStream.toByteArray());
	                } catch (IOException e1) {
	                    Clients.prt("������ �����");
	                }
	                MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
	                        .setMimeType("image/jpeg").setTitle(title).build();
	                
	                IntentSender intentSender = Drive.DriveApi
	                        .newCreateFileActivityBuilder()
	                        .setInitialMetadata(metadataChangeSet)
	                        .setInitialContents(res.getContents())
	                        .build(MainActivity.mGoogleApiClient);
	                
	                 
	                try {
	                   ((Activity)context).startIntentSenderForResult(
	                            intentSender, 0, null, 0, 0, 0);
	                } catch (Exception e) {
	                	e.printStackTrace();
	                }
	            }
	        });
				

	}
	
	
/////������� ���� � �����//////////////////////////////////////////////////////////
	void downlodFileFromDisk(DriveId id)
	{   DriveFolder folder = Drive.DriveApi.getFolder(mGoogleApiClient, id);
	    
		Query query = new Query.Builder()
                 .addFilter(Filters.and(
                 (Filters.eq(SearchableField.MIME_TYPE, "image/png")),
                 (Filters.eq(SearchableField.TRASHED, false)))).build();	
		
		
		folder.queryChildren(mGoogleApiClient, query).setResultCallback(
				new ResultCallback<MetadataBufferResult>() {
	        @Override
	        public void onResult(MetadataBufferResult result) {
	        	if (result.getMetadataBuffer().getCount() == 0) {Clients.prt("����� ��������");}
	        	else {
	        		Materials.imagesFromDisk.clear();
	        		DriveHelper.countImage = result.getMetadataBuffer().getCount();
	        		DriveHelper.currentCountImage = 0;
	        		getProgressBar(context);
	           for(int i =0; i<result.getMetadataBuffer().getCount(); i++)
	           {    
	        	   DriveFile file = Drive.DriveApi.getFile(mGoogleApiClient, result.getMetadataBuffer().get(i).getDriveId());
	        	   file.getMetadata(mGoogleApiClient).setResultCallback(
                      new ResultCallback<MetadataResult>()  {
                      @Override
				     	public void onResult(MetadataResult result) {
                    	   Materials.imagesTitleFromDisk.add(result.getMetadata().getTitle());  
                        }
                      }
                    );
	        	   
	        	   file.openContents(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null).setResultCallback(
                       new ResultCallback<ContentsResult>() {
                  		@Override
	     				public void onResult(ContentsResult result) {
                           
                           Materials.imagesFromDisk.add(BitmapFactory.decodeStream(result.getContents().getInputStream()));	
                           DriveHelper.currentCountImage++;                          
                           if (currentCountImage == countImage) {DriveHelper.isDownloadFiles = true;}
				          }    
					
                      }
	        			   
	        	   );
	        	   
	        	}
	         }
	        	

	       }
	    });
		
	}
	
	
/////����� ��������-���� ��� ����������� �������� ������////////////////////////////////////
	void getProgressBar(Context context)
	{   h = new Handler();
		pd = new ProgressDialog(context);
		pd.setTitle("�������� �����������...");
		pd.setCancelable(true);
		pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		pd.setMax(countImage);
		Clients.prt("���� ��������  "+pd.getMax());
		pd.show();
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (pd.getProgress() <= countImage)
				{   
					h.post(new Runnable() {

						@Override
						public void run() {
							pd.setProgress(DriveHelper.currentCountImage);
							
						}});
					if (pd.getProgress() == countImage) {pd.dismiss();}
				}
				
			}}).start();

	}
 
}
