package com.example.mathdirectory.materials;


import java.io.File; 
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.mathdirectory.R;
import com.example.mathdirectory.clients.Clients;
import com.google.android.gms.drive.DriveId;


public class Materials extends Fragment implements OnClickListener
{   
	
	Button sendImageButton, getImageButton, test2, add_metod, add_variant;
	TextView titleForm;
	GridView gv;
	
	DriveHelper dh;
	Activity thisactivity;
	Spinner spinner_metod, spinner_variant;
	ProgressDialog pd;
	Handler h; 
	
	static public ArrayList<String> metodsList,variantsList;
	static public ArrayList<DriveId> variantsListDriveID;
	static public ArrayList<Bitmap> imagesFromDisk;
	static public ArrayList<String> imagesTitleFromDisk;
	
 
/////////////////////////////////////////////////////////////////////////////////////////////////////////
//�������� ��������� 
	
	 public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState) {
	          
		       View view = inflater.inflate(R.layout.material, null);	
		       DisplayMetrics metrics = new DisplayMetrics();
		       this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);		       
		       thisactivity = this.getActivity();
		       
		       metodsList = new ArrayList<String>();
		       variantsList = new ArrayList<String>();
		       variantsListDriveID = new ArrayList<DriveId>();
		       imagesFromDisk = new ArrayList<Bitmap>();
		       imagesTitleFromDisk = new ArrayList<String>();
		       
		       sendImageButton = (Button)view.findViewById(R.id.sendimage);
		       getImageButton = (Button)view.findViewById(R.id.getimage);
		       test2 = (Button)view.findViewById(R.id.test2);
		       sendImageButton.setWidth(metrics.widthPixels/3);
		       getImageButton.setWidth(metrics.widthPixels/3);
		       test2.setWidth(metrics.widthPixels/3);
		       titleForm = (TextView)view.findViewById(R.id.titleform);
		       titleForm.setTextColor(Color.BLACK);
		       gv = (GridView)view.findViewById(R.id.gridView1);
		       
		       sendImageButton.setOnClickListener(this);
		       getImageButton.setOnClickListener(this);
		       test2.setOnClickListener(this);			   
		       
		       Clients.prt("������ "+metrics.heightPixels+", ������ "+metrics.widthPixels);
		       
		        dh = new DriveHelper(thisactivity);
		       		       	   
			   return view;	    
			   		  }

	 
	 
///////////////////////////////////////////////////////////////////////////////////////////////////////
//��������� �������
	 
	@Override
	public void onClick(View v) {
	   switch(v.getId())
	   {
	   
///////////////////////////////////////////
	    
	   case R.id.getimage:
	   {   dh.getMetodsList("�����", 1);
		   
		   AlertDialog.Builder selectMetodDialog = new AlertDialog.Builder(thisactivity);
	       selectMetodDialog.setTitle("�������� ���� � �������");

	       View selectmaterial_form = (View)thisactivity.getLayoutInflater().
				   inflate(R.layout.materials_selectmater_dialog, null);
	       selectMetodDialog.setView(selectmaterial_form);
	       
	         
	       add_metod = (Button)selectmaterial_form.findViewById(R.id.add_metod);
	       add_metod.setOnClickListener(this);
	       add_variant = (Button)selectmaterial_form.findViewById(R.id.add_variant);
	       add_variant.setOnClickListener(this);
	       
	       
///////////��������� �������� ���������////////////////////////////////////////////////////////
	       spinner_metod = (Spinner)selectmaterial_form.findViewById(R.id.dialodSpinnerMetod);      

				ArrayAdapter<String> adaptermetod = new ArrayAdapter<String>(thisactivity, 
			    		   android.R.layout.simple_list_item_1, metodsList);
				adaptermetod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
			       spinner_metod.setAdapter(adaptermetod);    
			       
	       spinner_metod.setOnItemSelectedListener(clickItemMetodPos);
	       
/////////////��������� �������� ���������///////////////////////////////////////////////////////
	       
	        spinner_variant = (Spinner)selectmaterial_form.findViewById(R.id.dialogSpinnerVariant);         
            spinner_variant.setOnItemSelectedListener(clickItemVariantPos);
	       
	     	       	       
	       ////////////////������ "�������"
	       selectMetodDialog.setPositiveButton("�������", new DialogInterface.OnClickListener() {
			
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			    	DriveHelper.isDownloadFiles = false;
			    	dh.downlodFileFromDisk(variantsListDriveID.get(spinner_variant.getSelectedItemPosition()));			        
			        refreshGrid();
			        titleForm.setText("����: "+
			        		metodsList.get(spinner_metod.getSelectedItemPosition())+" �������: "+
			        		   variantsList.get(spinner_variant.getSelectedItemPosition()));
                    
		     	}
			    
		   }); 
	       
	       /////////////////������ "������"
	       selectMetodDialog.setNegativeButton("������", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				
			} 
		});
	       
	       selectMetodDialog.create().show();   
		   break;
	   }
	    
////////////////////////////////////////////
	   case R.id.sendimage:
	      { 
	    	  new FileManager(thisactivity, dh); 
		break;
	      }
	      
////////////////////////////////////////////	   
	   case R.id.test2:
	      {  
	
	    	 break;
	      }

///////////////////////////////////////////
	   case R.id.add_metod:
     	  {
     	   getDialodAdding("�������� ���������", "�����");
     	   break;
	      }

//////////////////////////////////////////
	   case R.id.add_variant:
	      {
	       getDialodAdding("�������� �������", this.spinner_metod.getSelectedItem().toString());
		   break;
	      }
	      
	   }
		
	}
	
	
/////��������� ��� ��������� ������� �� ������� ���������/////////////////////////////////
	AdapterView.OnItemSelectedListener clickItemMetodPos = new AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			
			dh.getMetodsList(spinner_metod.getSelectedItem().toString(), 2);
			overloadSpinner();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
			
		}};
	
		
	OnItemSelectedListener clickItemVariantPos = new AdapterView.OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			
		}
	};

	public void overloadSpinner()
	{		ArrayAdapter<String> adaptervariant = new ArrayAdapter<String>(thisactivity, 
	    		   android.R.layout.simple_list_item_1, variantsList);
		adaptervariant.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		spinner_variant.setAdapter(adaptervariant);    
	}
	

////����� ������� ��� ���������� ����� ////////////////////////////////////////////
	void getDialodAdding(String nameDialogTitle, final String nameSourseFolder) {
	AlertDialog.Builder addMetodDialog = new AlertDialog.Builder(thisactivity);
       addMetodDialog.setTitle(nameDialogTitle);
        
       final EditText et = new EditText(thisactivity);   	          
       addMetodDialog.setView(et);
       
       addMetodDialog.setPositiveButton("��������", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			dh.createMaterialFolder(et.getText().toString(), nameSourseFolder);
		}
	}); 
       
       addMetodDialog.setNegativeButton("������", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();					
		}
	});
       
       addMetodDialog.create().show();
	}
	
	
/////�������� �������� ������ ��� ������� �����//////////////////////////////////////////////
	void refreshGrid()
	{
   	 final Handler myHandler = new Handler()
	  {
		  @Override
		  public void handleMessage(Message msg) {
			  gv.setAdapter(new CustomGridViewImages(thisactivity, imagesFromDisk, imagesTitleFromDisk)); 
		  }

	  };
	  
	 new Thread(new Runnable() {

		@Override
		public void run() {
			while (!DriveHelper.isDownloadFiles)
			{
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			Message msg = myHandler.obtainMessage();
           myHandler.sendMessage(msg);                 		
		}}).start(); 
	}
	
	

	
	
  /////////////////////////////////////////////////////////////////////////////////////////
 ///��������� ����� ��� ������ ��������� ���������////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
	static class FileManager
{
	ArrayAdapter<String> adapter;
	Context context;
	DriveHelper drivehelper;
	File[] files = Environment.getExternalStorageDirectory().listFiles();
	List<File[]> tempFiles = new ArrayList<File[]>();
	List<String> tempTitles = new ArrayList<String>();
	ListView lv;
	TextView tv;
	
	FileManager(Context con, DriveHelper dh)
	{   context = con;
	    this.drivehelper = dh;
		adapter = new  ArrayAdapter<String> (con.getApplicationContext(),
			android.R.layout.simple_list_item_1);
		createFileBrowserDialog();
	} 
	
	private void createFileBrowserDialog()
	  {	View viewContentDialog = ((Activity)context).getLayoutInflater().inflate(R.layout.materials_filebrowser_dialog, null); 
		lv = (ListView)viewContentDialog.findViewById(R.id.listView_filebrowserdialog);
		tv  = (TextView)viewContentDialog.findViewById(R.id.textView_filebrowserdialog);
		Button b = (Button)viewContentDialog.findViewById(R.id.buttonback_filebrowserdialog);
		lv.setAdapter(getFileAdapter());
		lv.setOnItemClickListener(listener);
		b.setOnClickListener(new View.OnClickListener() {
			//���� �� ������ �����
			@Override
			public void onClick(View v) {
				if (tempFiles.size() != 0)
				  { files = tempFiles.get(tempFiles.size()-1);
				    tempFiles.remove(tempFiles.size()-1);
				    tv.setText(tempTitles.get(tempTitles.size()-2));
				    tempTitles.remove(tempTitles.size()-1);
					lv.setAdapter(getFileAdapter());
					}	
			}
		});
		tv.setText(Environment.getExternalStorageDirectory().getAbsolutePath());
		tempTitles.add(Environment.getExternalStorageDirectory().getAbsolutePath());
		
		AlertDialog.Builder builder = new Builder(context);
		builder.setTitle("�������� �����");
		builder.setView(viewContentDialog);
		builder.setNegativeButton("������", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				
			}
		});
		builder.create();
		builder.show();
	  } 
	
	private ListAdapter getFileAdapter() {		
		adapter.clear();
		for (int i = 0; i < files.length; i++)
		{   if ((isTypeImage(files[i])) || files[i].isDirectory() )
			{adapter.add(files[i].getName());}
		}
		return adapter;
	}
	
/////���������� ������� �� ������ ������////////////////////////////////////////////////////////////	
	AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener(){

		@Override
		public void onItemClick(AdapterView<?> arg0, View v, int position,
				long id) {
			if (files[position].isDirectory())
		    	{tempFiles.add(files);			
		    	tv.setText(files[position].getAbsolutePath());
		    	tempTitles.add(files[position].getAbsolutePath());
		    	files = files[position].listFiles();
			    lv.setAdapter(getFileAdapter());}
			else 
			    {
			    drivehelper.uploadFileToDrive(getBitmapfromSD(files[position].getAbsolutePath()), files[position].getName());
			    }
				
			   
		}};
	
////jpeg-�� �� �������� ����?///////////////////////////////////////////////////////////////////////	
	boolean isTypeImage(File file)
	{   boolean result = false;
	String type = null;
    String extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(file).toString());
    if (extension != null) {
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        type = mime.getMimeTypeFromExtension(extension);
        if (type != null)
           {if (type.equals("image/jpeg")) {result = true;}}
    }
    
		return result;
	}
}

////�������� Bitmap �� SD �����////////////////////////////////////////////////////////////////////
	private static Bitmap getBitmapfromSD(String path)
    {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeFile(path, options);
		return bitmap;
	}

	
	
}
	
	
	
	

