package com.example.mathdirectory.materials;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.provider.MediaStore.Images;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.example.mathdirectory.R;
import com.example.mathdirectory.clients.Clients;

public class CustomGridViewImages extends BaseAdapter{

	private Context mContext;
	ArrayList<Bitmap> images;
	ArrayList<String> title;

	public CustomGridViewImages(Context c, ArrayList<Bitmap> b, ArrayList<String> s) {
		mContext = c;
		this.images = b;
		this.title = s;
	}

	public int getCount() {
		return images.size();
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater lInf = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = lInf.inflate(R.layout.materials_customimage, null);
		ImageView i = (ImageView)v.findViewById(R.id.customimage_imageview);
		CheckBox cb = (CheckBox)v.findViewById(R.id.customimage_checkbox);
        i.setImageBitmap(images.get(position));
        cb.setText(title.get(position));
		return v;
	}




}
