package com.example.mathdirectory;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MyDB   {
	
	private DBHelper dbhelper;
	Context context;
	public List<Integer> tempID = new ArrayList<Integer> ();
	
	SQLiteDatabase db;
	public static final String CLIENT_TABLE_NAME = "clients", ORDER_TABLE_NAME = "orders";
	
	public MyDB(Context context)
	{this.context = context;
	dbhelper = new DBHelper(context);}
	
/////////////////////////////////////////////////////////////////////	
	/**
	 * ����� �������� ����� � ������� �� ������� ��������
	 * 
	 */
	public List<String> getNameListFromClientsTable()
	{   ArrayList<String> result = new ArrayList<String>();
	    SQLiteDatabase db = dbhelper.getReadableDatabase();
	    Cursor cur = this.getAllDataFromTable(CLIENT_TABLE_NAME);
	    cur.moveToFirst();		
	       while (!cur.isAfterLast())   {
	    	      result.add(cur.getString(1)+" "+cur.getString(2));
	    	      tempID.add(cur.getInt(0));
	    	      cur.moveToNext();
	              
	       }
	       db.close();
		return result;}
	
////////////////////////////////////////////////////////////////////////
	
	public Cursor getAllDataFromTable(String tablename) {
		db = this.getReadDB();
		return db.query(tablename, null, null, null, null, null, null); 
		
	}
/////////////////////////////////////////////////////////////////////////
	
	/**
	 * 
	 * @return �� ��� ������
	 */
	public SQLiteDatabase getReadDB()
	{   
		db = dbhelper.getReadableDatabase();
		return db;
	}
	
	/**
	 * 
	 * @return �� ��� ������
	 */
	public SQLiteDatabase getWriteDB()
	{   db = dbhelper.getWritableDatabase();
		return db;
	}
	
	/**
	 * 
	 * @return �������� ��
	 */
	public void close()
	{
		db.close();
	}


	
}
