package com.example.mathdirectory.clients;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mathdirectory.DBHelper;
import com.example.mathdirectory.MyDB;
import com.example.mathdirectory.R;

public class Clients extends Fragment implements OnClickListener, 
                                                 OnItemClickListener, 
                                                 Animation.AnimationListener, 
                                                 View.OnTouchListener{
	Context context;
	ListView list_of_clients;	
	Button b_add, b_edit, b_delete;
	View mainview, filterview, closeopen_filter, search_filter;
	TextView searchET;
	String clientIDdatabase;
	ArrayAdapter<String> aa;
	private String[] name_field = new String[] {"name", "surname", "phone", "vklink", "adress"};
	static int current_id, current_listview_index = -1;
	TextView[] datafields;  
	MyDB mydb;
	boolean isFilterClose;
	Animation animup, animdown; 
	
	
	android.widget.RelativeLayout.LayoutParams paramsLV, paramsFilter;
	
	RelativeLayout rl; 
	 
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		      Bundle savedInstanceState) {
		
		        View mainview = inflater.inflate(R.layout.client, null);
		        mainview.setOnTouchListener(this);
		        current_id = 0;
		        
		        context = getActivity().getApplicationContext();
		       
		        list_of_clients = (ListView)mainview.findViewById(R.id.listClient);
		        registerForContextMenu(list_of_clients);
		        list_of_clients.setOnItemClickListener(this);
		        paramsLV = new RelativeLayout.LayoutParams(list_of_clients.getLayoutParams());
		        
		        b_add = (Button)mainview.findViewById(R.id.addClient);
		        b_add.setOnClickListener(this);
		        b_edit = (Button)mainview.findViewById(R.id.clients_editclient_button);
		        b_edit.setOnClickListener(this);
		        b_delete = (Button)mainview.findViewById(R.id.clients_deleteclient_button);
		        b_delete.setOnClickListener(this);
		        
		        datafields = new TextView[] 
		    			{(TextView)mainview.findViewById(R.id.ctvvalue_name),
		    			(TextView)mainview.findViewById(R.id.ctvvalue_surname),
		    			(TextView)mainview.findViewById(R.id.ctvvalues_phone),
		    			(TextView)mainview.findViewById(R.id.ctvvalues_VK),
		    			(TextView)mainview.findViewById(R.id.ctvvalues_address)};
		        datafields[3].setOnClickListener(this);
		        
                rl = (RelativeLayout)mainview.findViewById(R.id.clietnsRL);	
                           
                updateFilter();
                animup = AnimationUtils.loadAnimation(getActivity(), R.anim.filterup);	
                animup.setAnimationListener(this);
    		    animdown = AnimationUtils.loadAnimation(getActivity(), R.anim.filterdown); 
    		    animdown.setAnimationListener(this);
		        
		        mydb = new MyDB(getActivity().getApplicationContext());
		        
		        refreshListView(mydb.getNameListFromClientsTable());
		        
				return mainview;		    
		  }


/////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
		
//////////////////////////////		
//������ ��������
		case R.id.addClient:
		   {   
			   createClientDialog(true);
			   break;
		   }
////////////////////////
//������� �� ������ ������
		case R.id.clietns_search_filters:
		   {			    
			   String searchTemp = searchET.getText().toString();
			   if (searchTemp.equals("")) {this.refreshListView(mydb.getNameListFromClientsTable());}
			   else {	   
			   Cursor cur = mydb.getReadDB().rawQuery("SELECT id, name, surname FROM clients where surname like '"+searchTemp+"%' or name like '"+searchTemp+"%'"  , null);			 			   		   
			   ArrayList<String> clientsALquery = new ArrayList<String>();
			   mydb.tempID.clear();
			   cur.moveToFirst();		
				while (!cur.isAfterLast())
				    {
					
					clientsALquery.add(cur.getString(1)+" "+cur.getString(2));
					mydb.tempID.add(cur.getInt(0)); 
					cur.moveToNext();
					}
				mydb.close();
				refreshListView(clientsALquery);
				}
			   break;  
		   }
		   
   /////////////////////
   ///������/������� ������
		case R.id.clients_filter_image:
		{     		    
			if (isFilterClose)
			    {filterview.startAnimation(animdown); 
			    isFilterClose = false;}			
			else {filterview.startAnimation(animup); isFilterClose = true;}
					
			break; 
		}
		
  //////////////////////////
  ////������� �� ������ � �������
		case R.id.ctvvalues_VK:
		{   if (!datafields[3].equals("")) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://vk.com/"+datafields[3].getText().toString()));
			super.startActivityForResult(intent, 0); }
			break;
		}
		
   /////////////////////////////
   ///������� �� ������ "�������"
    case R.id.clients_deleteclient_button:
    {
    	if (current_id != 0)
    	{
    		if (current_id != 0)
        	{mydb.getWriteDB().delete("clients", "id = "+current_id, null);
        	refreshListView(mydb.getNameListFromClientsTable());
        	mydb.close();
            for (int i = 0; i < datafields.length; i++)
               {
            	datafields[i].setText("");
               }
            Clients.current_listview_index = -1;
        	}		
        	break;
    	}
    }
    
    //////////////////////////////
    ///������� �� ������ "�������������"
    case R.id.clients_editclient_button:
    {
    	createClientDialog(false);
    	break;
    }
			
		default: break;
		}
				
	} 
	  

	
//////////////////////////////////////////////////////////////////////////////////////////////////
//��������� ������� �� ������� ListView	
	@Override
	public void onItemClick(AdapterView<?> av, View v, int index, long id) {
		int toppos = list_of_clients.getFirstVisiblePosition();	
		Clients.current_listview_index = index;
		Clients.prt("������ "+mydb.tempID.get(index));
		Cursor cur = mydb.getReadDB().rawQuery("SELECT * from clients where id = "+mydb.tempID.get(index), null);
		cur.moveToFirst();
		for (int i = 0; i <= 4; i++)	
		  {Clients.prt("������ "+cur.getString(i+1));
			datafields[i].setText(cur.getString(i+1));
		  current_id = cur.getInt(0);
		  }		
		mydb.close();
		refreshListView(mydb.getNameListFromClientsTable());
		list_of_clients.setSelection(toppos);
	}
		
	
///////////////////////////////////////////////////////////////////////////////////////////////////	
//���������� ������	

	/**
	 * 
	 * 
	 * @param al ������
	 */
	void refreshListView(List<String> al)
	{   aa = new MyOwnAdapter(getActivity()
        		.getApplicationContext(), al);
        list_of_clients.setAdapter(aa);
	}
	
	
///�������� ����������� ���� ��� ����������
///� �������������� �������
	private void createClientDialog(final boolean isAdding)
	{   String action;
		AlertDialog.Builder addclient_dialog = new AlertDialog.Builder(this.getActivity());
		if (isAdding) {action = "��������";} else {action = "��������";}  
		addclient_dialog.setTitle(action+" �������");
		   View addcleint_form = (View)this.getActivity().getLayoutInflater().
				   inflate(R.layout.client_addclient, null);
		   addclient_dialog.setView(addcleint_form);
		   
		   final EditText[] fieldET = new EditText[5];			   
		   fieldET[0] = (EditText)addcleint_form.findViewById(R.id.name_et);
		   fieldET[1] = (EditText)addcleint_form.findViewById(R.id.surname_et);
		   fieldET[2] = (EditText)addcleint_form.findViewById(R.id.phone_et);
		   fieldET[3] = (EditText)addcleint_form.findViewById(R.id.vklink_et);
		   fieldET[4] = (EditText)addcleint_form.findViewById(R.id.adress_et);
		   if (!isAdding)
		   {
			   for (int i = 0; i < fieldET.length; i++)
			   {
				   fieldET[i].setText(datafields[i].getText());
			   }
		   }
		   
		   addclient_dialog.setPositiveButton(action, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (fieldET[1].getText().toString().equals("")) 
				   {
					Toast.makeText(getActivity().getApplicationContext(), "���� ������� ����������� ��� ����������", Toast.LENGTH_SHORT).show();
				   }
				else {
					ContentValues cv = new ContentValues();
		            for (int i = 0; i < fieldET.length; i++)
		            cv.put(name_field[i], fieldET[i].getText().toString());  
					if (isAdding)
				             { prt("��������� ������ "+mydb.getReadDB().insert(MyDB.CLIENT_TABLE_NAME, null, cv)); }
					else { mydb.getReadDB().update("clients", cv, "id = "+current_id, null);       }
					mydb.close();
	         		refreshListView(mydb.getNameListFromClientsTable());
				}
			}
		});
		   			    
		   addclient_dialog.setNegativeButton("������", new DialogInterface.OnClickListener() {
	
				@Override
				public void onClick(DialogInterface dialog, int which) {
					for (int i = 0; i < fieldET.length; i++)
					{
						fieldET[i].setText("");
					}					
					dialog.cancel();
				}
			});
		    
		   addclient_dialog.create();
		   addclient_dialog.show();
	}

	
	
	static public void prt(String s)
	{System.out.println(s);}

	static public void prt(int s)
	{System.out.println(""+s);}

	static public void prt(boolean s)
	{System.out.println(s);}

	
////��������� ���������� �������////////////////////////////////////////////////////////////////////	
	void updateFilter()
	{
		LayoutInflater li = (LayoutInflater)getActivity().getSystemService
	      	      (Context.LAYOUT_INFLATER_SERVICE);
			filterview = li.inflate(R.layout.clients_filters, null);
			filterview.setId(1111);
	
			
			android.widget.RelativeLayout.LayoutParams params = 
					new RelativeLayout.LayoutParams(paramsLV.width, 
							LayoutParams.WRAP_CONTENT);
			closeopen_filter = filterview.findViewById(R.id.clients_filter_image);
			closeopen_filter.setOnClickListener(this);
			search_filter = filterview.findViewById(R.id.clietns_search_filters);
			search_filter.setOnClickListener(this);
			searchET = (TextView)filterview.findViewById(R.id.clients_ETsearch_filters);

			paramsLV.addRule(RelativeLayout.BELOW, filterview.getId());	
			paramsLV.addRule(RelativeLayout.ABOVE, b_add.getId());
            list_of_clients.setLayoutParams(paramsLV);

	        rl.addView(filterview, params);

	}


@Override
public void onAnimationEnd(Animation animation) {
	
	if(animation == animup)
	    {filterview.setY(-58);}
	else if(animation == animdown) 
	    {filterview.setY(0);}
	
	
}


@Override
public void onAnimationRepeat(Animation animation) {}
@Override
public void onAnimationStart(Animation animation) {}


@Override
public boolean onTouch(View v, MotionEvent event) {
	switch (event.getAction())
	{
	case MotionEvent.ACTION_MOVE:
	   {
		   Clients.prt("���� ���������!");
		   break;
	   }
	case MotionEvent.ACTION_DOWN:
	   {
		   Clients.prt("��� �� ����");
		   break;
	   }
	case MotionEvent.ACTION_UP:
	   {
		   Clients.prt("��! �����-��");
		   break;
	   }
	default: break;
	}
	return false;
}


}


 
