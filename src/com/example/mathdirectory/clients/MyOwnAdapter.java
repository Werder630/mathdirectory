package com.example.mathdirectory.clients;

import java.util.ArrayList;
import java.util.List;

import com.example.mathdirectory.R;
import com.example.mathdirectory.R.id;
import com.example.mathdirectory.R.layout;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyOwnAdapter extends ArrayAdapter<String> {

	  private final Context context;
	    private final List<String> values;
	    TextView surname;
	    LayoutInflater inflater;
	    
	    public MyOwnAdapter(Context context, List<String> values) {
	        super(context, R.layout.clients_row, values);
	        this.context = context;
	        this.values = values;
	        inflater = (LayoutInflater) context
	                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    }
	    
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent)
	    {   
	    	
	    	 View rowView = inflater.inflate(R.layout.clients_row, parent, false);
			 surname = (TextView)rowView.findViewById(R.id.fullname_listview);		
			 if (position == Clients.current_listview_index) 
			     {surname.setTextColor(Color.BLACK);}
			 surname.setText(values.get(position));
	    	 return rowView;	
	    	 
	    	 
	    }
	    
	
	    
	    


}
